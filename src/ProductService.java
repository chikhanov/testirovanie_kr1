import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
public class ProductService {

    public Map<Food, Integer> filter(Bucket bucket, Set<FoodType> types) {

        return bucket.getProducts().entrySet().stream()
                .filter(item -> types.contains(item.getKey().getType()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
