/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
public enum FoodType {
    fruit, vegetable, diary, flour, meat, fish
}
