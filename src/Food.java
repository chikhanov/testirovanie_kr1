/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
public class Food {

    private FoodType type;

    private String name;

    public Food(String name, FoodType type) {
        this.name = name;
        this.type = type;
    }

    public FoodType getType() {
        return type;
    }

    public void setType(FoodType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
