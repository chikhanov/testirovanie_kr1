import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
public class Bucket {

    private Map<Food, Integer> products = new HashMap<>();

    public void addProduct(Food product, int number) {
        Integer currentNumber = this.products.get(product);
        if (currentNumber != null)  {
            this.products.put(product, number + currentNumber);
        } else {
            this.products.put(product, number);
        }
    }

    public void removeProduct(Food product, int number) {
        Integer currentNumber = this.products.get(product);
        if (currentNumber == null) {
            throw new RuntimeException("Product isn`t present in bucket");
        } else {
            if (currentNumber > number) {
                this.products.put(product, currentNumber - number);
            } else {
                this.products.remove(product);
            }
        }
    }

    public Map<Food, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Food, Integer> products) {
        this.products = products;
    }
}
