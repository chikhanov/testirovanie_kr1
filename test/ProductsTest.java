/**
 * Created by Dmitriy Chikhanov on 02.10.16.
 */
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static junit.framework.TestCase.assertEquals;

public class ProductsTest {
    @Rule
    public TestRule watcher = new TestWatcher() {
        protected void starting(Description description) {
            System.out.println("Starting test: " + description.getMethodName());
        }
    };

    @Test
    public void filterBucketTest() {
        Food potato = new Food("Potato", FoodType.vegetable);
        Food cucumber = new Food("Cucumber", FoodType.vegetable);
        Food beef = new Food("Beef", FoodType.meat);
        Food tuna = new Food("Tuna", FoodType.fish);
        Food salmon = new Food("Salmon", FoodType.fish);
        Food apple = new Food("Apple", FoodType.fruit);
        Food orange = new Food("Orange", FoodType.fruit);

        // Наполняем корзину
        Bucket bucket1 = new Bucket();
        bucket1.addProduct(potato, 10);
        bucket1.addProduct(cucumber, 20);
        bucket1.addProduct(apple, 40);
        bucket1.addProduct(tuna, 100);
        bucket1.addProduct(salmon, 40);
        bucket1.addProduct(orange, 15);
        bucket1.addProduct(beef, 15);

        ProductService productService = new ProductService();

        // Фильтруем корзину
        HashSet<FoodType> filter =  new HashSet<>();
        filter.addAll(Arrays.asList(FoodType.fish, FoodType.meat));
        bucket1.setProducts(productService.filter(bucket1, filter));

        // Собираем заглушку
        HashMap<Food, Integer> filteredProducts = new HashMap<>();
        filteredProducts.put(tuna, 100);
        filteredProducts.put(salmon, 40);
        filteredProducts.put(beef, 15);

        // Сравниваем результаты
        assertEquals(filteredProducts, bucket1.getProducts());

    }
}
